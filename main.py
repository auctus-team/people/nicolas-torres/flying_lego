import sys

import numpy as np
from matplotlib import pyplot as plt
from mpl_toolkits.mplot3d import Axes3D

np.set_printoptions(precision=2)
np.set_printoptions(suppress=True)


def create_axis(ax, maxval):
    one = [0, maxval]
    origin = [0, 0]
    ax.plot(one, origin, origin, color='red', linewidth=4)
    ax.plot(origin, one, origin, color='green', linewidth=4)
    ax.plot(origin, origin, one, color='blue', linewidth=4)


class Lego:
    def __init__(self, p0, v0, yintersection, zintersection):
        try:
            if len(p0) != 3:
                raise TypeError
        except TypeError:
            sys.exit('p0 must be a 3-vector/list')

        try:
            if len(v0) != 3:
                raise TypeError
        except TypeError:
            sys.exit('v0 must be a 3-vector/list')

        p0 = np.array(p0)
        v0 = np.array(v0)

        self.p0, self.v0, self.yintersection, self.zintersection = p0, v0, yintersection, zintersection

        self.p = np.array([p0])

        dt = 1e-3
        vnext = v0
        g = 9.81
        acc = np.array([0, 0, -g])

        def yintersect_cond(p):
            if v0[1] < 0 and yintersection < 0:
                return p[1] > yintersection
            return p[1] < yintersection

        while self.p[-1][2] > zintersection and yintersect_cond(self.p[-1]):
            vnext = vnext + dt * acc
            self.p = np.vstack(
                [self.p, self.p[-1] + vnext * dt + acc * dt * dt / 2])

    def maxval(self):
        return np.max(lego.p)

    def plot(self, ax, color='black'):

        ax.scatter(self.p[0, 0],
                   self.p[0, 1],
                   self.p[0, 2],
                   color=color,
                   marker='^',
                   s=100)
        ax.scatter(self.p[-1, 0],
                   self.p[-1, 1],
                   self.p[-1, 2],
                   color=color,
                   marker='x',
                   s=100)
        ax.plot(
            self.p[:, 0],
            self.p[:, 1],
            self.p[:, 2],
            color=color,
            linewidth=2,
            linestyle='dashed',
        )

        def limsdelt(p, ind, factorr):
            minlim = np.min(p[:, ind])
            maxlim = np.max(p[:, ind])
            maxfactor = (1 / factorr) if maxlim < 0 else factorr
            minfactor = (1 / factorr) if minlim > 0 else factorr
            min = minfactor * minlim
            max = maxfactor * maxlim
            delta = max - min
            return min, max, delta

        factor = 1.8
        minlimx, maxlimx, deltax = limsdelt(self.p, 0, factor)
        minlimy, maxlimy, deltay = limsdelt(self.p, 1, factor)
        minlimz, maxlimz, deltaz = limsdelt(self.p, 2, factor)
        # print('x', minlimx, '->', maxlimx, deltax)
        # print('y', minlimy, '->', maxlimy, deltay)
        # print('z', minlimz, '->', maxlimz, deltaz)

        steps = 10.
        #################################################################
        # z plane
        xx, yy = np.meshgrid(np.arange(minlimx, maxlimx, deltax / steps),
                             np.arange(minlimy, maxlimy, deltay / steps))
        normal = np.array([0, 0, 1])
        z = (self.zintersection + normal[0] * xx + normal[1] * yy) / normal[2]
        ax.plot_surface(xx, yy, z, color='blue', alpha=0.05)

        #################################################################
        # y plane
        xx, zz = np.meshgrid(np.arange(minlimx, maxlimx, deltax / steps),
                             np.arange(minlimz, maxlimz, deltaz / steps))
        normal = np.array([0, 1, 0])
        y = (self.yintersection + normal[0] * xx + normal[2] * zz) / normal[1]
        ax.plot_surface(xx, y, zz, color='green', alpha=0.1)


################################################################
# create figure and 3d
fig = plt.figure()
# previous matplotlib version
# ax = fig.add_subplot(111, projection='3d')
ax = Axes3D(fig)
# ax.scatter(0, 0, 0, color='blue', marker='^', s=100)

ax.set_xlabel('X', fontsize=20)
ax.set_ylabel('Y', fontsize=20)
ax.set_zlabel('Z', fontsize=20)
plt.grid()

################################################################
# for plotting a plane
# lim = 30
# for p in np.arange(0, lim, lim / 20):
#     ax.scatter(p, p, 0, color='green')
# xx, yy = np.meshgrid(np.arange(-lim, lim, lim / 10),
#                      np.arange(-lim / 2, lim / 2, lim / 10))
# # x + y -z = 15
# normal = np.array([1, 1, -1])
# z = (1 + normal[0] * xx + normal[1] * yy) / normal[2]
# z = (normal[0] * xx + normal[1] * yy) / normal[2]
# ax.plot_surface(xx, yy, z, alpha=0.5)
################################################################

yintersec, zintersec = 5, 1
lego = Lego(p0=[3, 2, 3],
            v0=[0.8, 2.8, 4.4],
            yintersection=yintersec,
            zintersection=zintersec)
lego.plot(ax)
lego2 = Lego(p0=[-1, 1.5, 3],
             v0=[-0.8, -1.3, -0.2],
             yintersection=yintersec,
             zintersection=zintersec)
lego2.plot(ax, color='green')
lego3 = Lego(p0=[1, 2, 5],
             v0=[0, 0.3, 4],
             yintersection=yintersec,
             zintersection=zintersec)
lego3.plot(ax, color='blue')

create_axis(ax, maxval=1.3 * lego.maxval())
create_axis(ax, maxval=1.3 * lego2.maxval())
create_axis(ax, maxval=1.3 * lego3.maxval())

plt.show()
