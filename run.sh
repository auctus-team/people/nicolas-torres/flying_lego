#!/usr/bin/env bash

if ! command -v yapf &> /dev/null
then
    echo "warn: yapf could not be found"
    if ! command -v yapf3 &> /dev/null
    then
        echo "warn: yapf3 could not be found"
        echo "      (will not format code)"
    else
        yapf3 -i *.py
    fi
else
    yapf3 -i *.py
fi

python main.py
